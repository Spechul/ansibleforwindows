# Ansible и конфигурация компьютеров с ОС Windows

## Нам потребуется:
- Компьютер с ОС Windows и настроенным SSH. Это наш целевой компьютер.
- Компьютер с возможностью запустить Ansible (Mac, компьютер с Linux, компьютер с как минимум Windows 1809 и включенным wsl/другие извращения, которые позволят нам запустить Ansible). Это наш "админский компьютер"

## Приводим к готовности Ansible на нашей админской машине (в моём случае - Windows с Ubuntu wsl)
Открываем консоль wsl и набираем следующие команды (не забываем `sudo apt-get update`, если wsl был запушен 1-й раз):

На чистом wsl не всегда есть штуки для прозвона сети, типа ifconfig, который может пригодиться, пока мы будем искать наш целевой компьютер, это стоит упомянуть.
```bash
sudo apt-get install sshpass
sudo apt-get install python3-pip
pip3 install ansible
```
*На заметку: не стоит устанавливать Ansible из пакетного менеджера, там как правило будет очень старая версия*

Далее, вероятно, из-за бага, придётся перезапустить wsl, т.к. он сразу не сможет подхватить Ansible.
Проверяем, есть ли у нас chocolatey (о нём позже)
```bash
ansible-galaxy collection | grep chocolatey
```
Если не нашлось - устанавливаем
```bash
ansible-galaxy install chocolatey.chocolatey
```

## Если мы в курсе, как работать с WinRM
Берём документацию отсюда [https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html](https://docs.ansible.com/ansible/latest/user_guide/windows_setup.html) и устанавливаем

## Если мы не в курсе, как работать с WinRM/хотим OpenSSH
Устанавливаем OpenSSH на целевую машину. Запускаем Powershell от имени Администратора и вводим команды:
```powershell
Add-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
(или) dism /Online /Add-Capability /CapabilityName:OpenSSH.Server~~~~0.0.1.0
Start-Service "sshd"
Start-Service "ssh-agent"
```
Также не забываем добавить правило фаервола, чтобы нас пустило подключиться
```powershell
netsh advfirewall firewall add rule name="SSHD service" dir=in action=allow protocol=TCP localport=22
```
Из-за того, что Windows при включении/выключении функций/установке обновлений/установке программ/в целом любит перезагружаться, желательно включить сервисы на автостарт, по крайней мере до момента, как мы закончим с конфигурацией скриптами Ansible
```powershell
Set-Service -Name "sshd" -StartType "Automatic"
Set-Service -Name "ssh-agent" -StartType "Automatic"
```
Для выключения - соответственно можно использовать на них
```powershell
Stop-Service "name-of-service"
Set-Service -Name "name-of-service" -StartType "Disabled"
```
Заходим в `%programdata%\ssh\` и редактируем файл конфигурации `sshd_config`. Ставим
- StrictModes no
- PublickeyAuthentication yes

Также, если хотим иметь powershell как стандартную консоль для подключения - добавляем
- Subsystem powershell c:/progra~1/powershell/7/pwsh.exe -sshs -NoLogo

Далее ставим пароль на свою админскую учётную запись, логинимся с админского компьютера и прокидываем ключи, **внимание**, сгенерированные в wsl. Для этого запускаем wsl, генерируем с помощью `ssh-keygen` и копируем их по пути `%programdata%\ssh\administrators_authorized_keys` c помощью `scp` (имеем в виду, что такой способ перетрёт файл на хосте, если он не был пустым)
```
ssh-keygen // enter, enter, enter
scp /home/your-wsl-user/.ssh/id_rsa.pub your-username@192.168.123.123:%programdata%/ssh/administrators_authorized_keys
```

## Что нам понадобится из возможностей Ansible
Полный список того, что предлагает ansible.windows - здесь https://galaxy.ansible.com/ansible/windows. Нам же понадобится только несколько модулей:
- win_chocolatey - пакетный менеджер
- win_optional_feature - обёртка для dism
- win_reboot - перезагрузка отдельной командой
- win_updates - модуль обновлений
- win_shell - модуль, позволяющий напрямую использовать консоль
- become - сменить пользователя, от имени которого будет выполняться действие

## И под конец
После того, как мы закончили с настройкой, надо прибраться за собой. Открываем в нашем настроенном компьютере powershell от имени администратора и удаляем SSH Server.
```powershell
Remove-WindowsCapability -Online -Name OpenSSH.Server~~~~0.0.1.0
netsh advfirewall firewall delete rule name="SSHD service"
```